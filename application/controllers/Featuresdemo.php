<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FeaturesDemo extends CI_Controller {

    public function index()
    {
        if ( ! file_exists(APPPATH.'views/pages/features_demo.php'))
            show_404();
        $this->load->view('pages/features_demo');
    }
}