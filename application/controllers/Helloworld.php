<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HelloWorld extends CI_Controller {
    public function index()
    {
        if ( ! file_exists(APPPATH.'views/pages/hello_world.php'))
            show_404();
        $this->load->view('pages/hello_world');
    }
}