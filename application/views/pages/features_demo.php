<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>CI Features Demo</title>
    </head>
    <body>
        <style>
        body {
            font-size: 1.3em; padding: 0 50px;
        }

        pre {
            border: 3px solid brown;
            padding: 1em;
        }
        </style>
        <?
        // Load các helper sử dụng trong này
        $this->load->helper('date');
        $this->load->helper('number');
        $this->load->helper('string');
        ?>

        <h1>Thư viện (Libraries):</h1>
        <h2>Benchmarking</h2>
        <p>CodeIgniter có một lớp luôn hoạt động, cho phép tính chênh lệch thời gian giữa hai điểm được đánh dấu. Dưới đây là ví dụ: </p>
        <pre>
            $this->benchmark->mark('code_start');
            // Đoạn code bắt đầu ở đây
            $a = 56;
            $b = 48;
            echo "Tìm ước chung lớn nhất của ".$a." và ".$b;
            if ($a == 0 || $b == 0)
                echo $a + $b;
            while ($a != $b)
            {
                if($a > $b)
                    $a = $a - $b;
                else
                    $b = $b - $a;
            }
            echo "&lt;p&gt;Kết quả: ".$a."&lt;/p&gt;";
            // Đoạn code kết thúc ở đây
            $this->benchmark->mark('code_end');
            echo "Thời gian thực hiện: ".$this->benchmark->elapsed_time('code_start', 'code_end')."s";
        </pre>
        <p><b>Kết quả:</b></p>
        <?
        $this->benchmark->mark('code_start');
        // Đoạn code bắt đầu ở đây
        $a = 56;
        $b = 48;
        echo "Tìm ước chung lớn nhất của ".$a." và ".$b;
        if ($a == 0 || $b == 0)
            echo $a + $b;
        while ($a != $b)
        {
            if($a > $b)
                $a = $a - $b;
            else
                $b = $b - $a;
        }
        echo "<p>Kết quả: ".$a."</p>";
        // Đoạn code kết thúc ở đây
        $this->benchmark->mark('code_end');
        echo "Thời gian thực hiện: ".$this->benchmark->elapsed_time('code_start', 'code_end')."s";
        ?>
        <br><br>
        <h1>Hàm trợ giúp (Helper functions):</h1>
        <h2>Date Helper</h2>
        <h3>now([$timezone = NULL])</h3>
        <pre>echo now(); <? echo "// In ra ".now(); ?></pre>
        <h3>unix_to_human([$time = ''[, $seconds = FALSE[, $fmt = 'us']]])</h3>
        <pre>echo unix_to_human(now()); <? echo "// In ra ".unix_to_human(now()); ?></pre>

        <h2>Number Helper</h2>
        <h3>byte_format($num[, $precision = 1])</h3>
        <pre>echo byte_format(45678, 2); <? echo "// In ra ".byte_format(45678, 2); ?></pre>

        <h2>String Helper</h2>
        <h3>random_string([$type = 'alnum'[, $len = 8]])</h3>
        <pre>echo random_string('alnum', 16); <? echo "// In ra ".random_string('alnum', 16); ?></pre>
        <h3>str_repeat($data[, $num = 1])</h3>
        <pre>echo str_repeat("CodeIgniter Example\n", 5);<br><? echo "// In ra: \n".str_repeat("CodeIgniter Example\n", 5) ?>
        </pre>
    </body>
</html>