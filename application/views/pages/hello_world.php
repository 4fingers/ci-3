<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hello World Example</title>
    </head>
    <body>
        <h1>Hello, world!</h1>
    </body>
</html>